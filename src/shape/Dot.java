/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

/**
 *
 * @author Olga
 */
public class Dot  {
    private int x;
    private int y;
    
    public Dot()  {
        x = 0; 
        y = 0;
    }
    
    public Dot(int x, int y)  {
        this.x = x;
        this.y = y;
    }
    
    public void setX(int x)  {
        this.x = x;
    }
    
    public void setY(int y)  {
        this.y = y;
    }
       
    public int getX()  {
        return this.x;
    }
    
    public int getY()  {
        return this.y;
    }
    
    @Override
    public boolean equals(Object o)  {
      if (null == o)
          return false;
      if (this == o)
          return true;
      if (getClass() != o.getClass())
	return false;
      Dot tmp = (Dot)o;
      return this.x == tmp.getX() && this.y == tmp.getY();
    }
    
    @Override
    public int hashCode()  {
        int result = 1;
        result += x*x + y*y;
        return result;
    }
    
    @Override
    public Dot clone()  {
        return new Dot(this.x, this.y);
    }
    
    @Override
    public String toString()  {
        String tmp;
        tmp = "(" + x + ";";
        tmp += getY()+ ")";
        return tmp;
    }
}
