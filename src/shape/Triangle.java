/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

import static shape.Shape.log;

/**
 *
 * @author Olga
 */
public class Triangle  {
    
    private Dot first;
    private Dot second;
    private Dot third;
    private double perimetr;
    private double square;
    private String typeOfMainCorner;
    private boolean isExist;
    
    public Triangle()  {
       first = new Dot();
       second = new Dot();
       third = new Dot();
       perimetr = 0;
       square = 0;
       typeOfMainCorner = null;
       isExist = false;
    }
    
    public Triangle(Dot first, Dot second, Dot third)  {
       this.first = first;
       this.second = second;
       this.third = third;
       isExist = type();
       if(!isExist)  {
           typeOfMainCorner = null;
           perimetr = 0;
           square = 0;
       }
    }
    
    public final boolean type()  {
        definePerimetr();
        defineSquare();
        defineCornerType();
        return isExist;
    }
    
    public double definePerimetr() {
        double lengthA = Math.sqrt(Math.pow(first.getX()-second.getX(),2)+Math.pow(first.getY()-second.getY(), 2));
        double lengthB = Math.sqrt(Math.pow(second.getX()-third.getX(),2)+Math.pow(second.getY()-third.getY(), 2));
        double lengthC = Math.sqrt(Math.pow(third.getX()-first.getX(),2)+Math.pow(third.getY()-first.getY(), 2));
        if(lengthA==0 || lengthB==0 || lengthC==0) {
            isExist = false;
            log.info("Can not construct trianga with this coordinates");
            return 0;
        }
        if(lengthA + lengthB == lengthC) {
            isExist = false;
            log.info("Can not construct trianga with this coordinates");
            return 0;
        }
        perimetr = lengthA + lengthB + lengthC;
        isExist = true;
        log.info("Perimetr: "+perimetr);
        return perimetr;
    }
    
    public double defineSquare() {
        double lengthA = Math.sqrt(Math.pow(first.getX()-second.getX(),2)+Math.pow(first.getY()-second.getY(), 2));
        double lengthB = Math.sqrt(Math.pow(second.getX()-third.getX(),2)+Math.pow(second.getY()-third.getY(), 2));
        double lengthC = Math.sqrt(Math.pow(third.getX()-first.getX(),2)+Math.pow(third.getY()-first.getY(), 2));
        if(lengthA==0 || lengthB==0 || lengthC==0) {
            isExist = false;
            return 0;
        }
        if(lengthA + lengthB == lengthC) {
            isExist = false;
            log.info("Can not construct trianga with this coordinates");
            return 0;
        }
        if(perimetr == 0) return 0;
        isExist = true;
        double p = perimetr/2;
        square = Math.sqrt(p*(p-lengthA)*(p-lengthB)*(p-lengthC));
        if(square == 0) {
            isExist = false;
            log.info("Can not construct trianga with this coordinates");
        }
        log.info("Square: "+square);
        return square;
    }
    
    public String defineCornerType()  {
        int vector1 = (first.getX()-second.getX())*(first.getY()-second.getY())+(second.getX()-third.getX())*(second.getY()-third.getY());
        int vector2 = (second.getX()-third.getX())*(second.getY()-third.getY())+(third.getX()-first.getX())*(third.getY()-first.getY());
        int vector3 = (third.getX()-first.getX())*(third.getY()-first.getY())+(first.getX()-second.getX())*(first.getY()-second.getY());
        if((vector1 == 0) || (vector2 == 0) || (vector3 == 0)) typeOfMainCorner = "Rectangular"; 
        else typeOfMainCorner = "Not rectangular"; 
        log.info("Main corner: "+typeOfMainCorner);
        return typeOfMainCorner;
    }
            
    public boolean init(int arr[])  {
        if(arr.length!=6) return false;
        else {
            int dotCounter = 1;
            for(int i=0; i<6; i++)  {
            if(i%2==0)  {
               if(dotCounter == 1) first.setX(arr[i]);
               else if(dotCounter == 2) second.setX(arr[i]);
               else third.setX(arr[i]);
            }
            else {
                if(dotCounter == 1) first.setY(arr[i]);
                else if(dotCounter == 2) second.setY(arr[i]);
                else third.setY(arr[i]);
                dotCounter++;
            }
            }
            type();
            return true;
       }
    }
    
    public void setFirst(Dot first)  {
        this.first = first;
    }
    
    public void setSecond(Dot second)  {
        this.second = second;
    }
    
    public void setThird(Dot third)  {
        this.third = third;
    }
     
    public Dot getFirst()  {
        return this.first;
    }
    
    public Dot getSecond()  {
        return this.second;
    }
    
    public Dot getThird()  {
        return this.third;
    }
    
    @Override
    public boolean equals(Object o)  {
      if (o == null)
          return false;
      if (this == o)
          return true;
      if (getClass() != o.getClass())
	return false;
      Triangle tmp = (Triangle)o;
      return this.first.equals(tmp.getFirst()) && this.second.equals(tmp.getSecond()) &&this.third.equals(tmp.getThird());
    }
    
    @Override
    public int hashCode()  {
        int result = 1;
        result += this.getFirst().hashCode() + this.getSecond().hashCode() +  this.getThird().hashCode();
        return result;
    }
    
    @Override
    public Triangle clone()  {
        return new Triangle(this.getFirst().clone(), this.getSecond().clone(), this.getThird().clone());
    }
    
    @Override
    public String toString()  {
        String tmp;
        if(typeOfMainCorner == null) tmp = "Can not construct trianga with this coordinates";
        else{
            tmp = typeOfMainCorner + "\nPerimetr: " + perimetr + "\nSquare: "  + square + "\nDot coordinates:\n";
        for(int i=0; i<3; i++)  {
            tmp += "точка " + (i+1);
            if(i+1 == 1) tmp += first.toString();
            else if(i+1 == 2) tmp += second.toString();
            else tmp += third.toString();
            tmp += "\n";
            }
        }
        return tmp;
    }
}
