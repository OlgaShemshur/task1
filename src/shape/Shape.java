/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

import java.io.FileReader;
import java.io.IOException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author Olga
 */
public class Shape {

    static{
        new DOMConfigurator().doConfigure("src\\shape\\log4j.xml", LogManager.getLoggerRepository());
    } 
    
    static Logger log = Logger.getLogger(Shape.class.getName());
    
    public static void main(String[] args) throws IOException{
        int [] arr = new int[6];
        try(FileReader reader = new FileReader("src\\resource\\input.txt"))
        {
            int c;
            int i = 0;
            while((c=reader.read())!=-1){ 
                if(i>6) throw new IOException();
                char k = (char)c;
                if(k==' ') continue;
                String s = ""+ k;
                arr[i] = Integer.parseInt(s);
                i++;
            } 
        }
        catch(IOException ex){
            log.error("Input error: ", ex);
        } 
        Triangle triangle = new Triangle();
        triangle.init(arr);
        System.out.print(triangle);
    }    
}
