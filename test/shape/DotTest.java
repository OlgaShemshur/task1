/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Olga
 */
public class DotTest {
    
    public DotTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setX method, of class Dot.
     */
     
    /**
     * Test of getX method, of class Dot.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Dot instance = new Dot(1,2);
        int expResult = 1;
        int result = instance.getX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getY method, of class Dot.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Dot instance = new Dot(1,2);
        int expResult = 2;
        int result = instance.getY();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Dot.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object o = new Dot(1,2);
        Dot instance = new Dot(1,2);
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Dot.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Dot instance = new Dot(1,2);
        int expResult = 6;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of clone method, of class Dot.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        Dot instance = new Dot(1,2);
        Dot expResult = new Dot(1,2);
        Dot result = instance.clone();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Dot.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Dot instance = new Dot(1,2);
        String expResult = "(1;2)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
