/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Olga
 */
public class TriangleTest {
    
    public TriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of type method, of class Triangle.
     */
    @Test
    public void testType() {
        System.out.println("type");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(1,-21), new Dot(0,4));
        boolean expResult = true;
        boolean result = instance.type();
        assertEquals(expResult, result);
    }

    /**
     * Test of definePerimetr method, of class Triangle.
     */
    @Test
    public void testDefinePerimetr() {
        System.out.println("definePerimetr");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        double expResult = 10.242640687119284;
        double result = instance.definePerimetr();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of defineSquare method, of class Triangle.
     */
    @Test
    public void testDefineSquare() {
        System.out.println("defineSquare");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        double expResult = 4.499999999999997;
        double result = instance.defineSquare();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of defineCornerType method, of class Triangle.
     */
    @Test
    public void testDefineCornerType() {
        System.out.println("defineCornerType");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        String expResult = "Прямоугольный";
        String result = instance.defineCornerType();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
    @Test
    public void testGetFirst() {
        System.out.println("getFirst");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        Dot expResult = new Dot(3,3);
        Dot result = instance.getFirst();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSecond method, of class Triangle.
     */
    @Test
    public void testGetSecond() {
        System.out.println("getSecond");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        Dot expResult = new Dot(0,3);
        Dot result = instance.getSecond();
        assertEquals(expResult, result);
    }

    /**
     * Test of getThird method, of class Triangle.
     */
    @Test
    public void testGetThird() {
        System.out.println("getThird");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        Dot expResult = new Dot(0,0);
        Dot result = instance.getThird();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Triangle.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object o = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Triangle.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        int expResult = 31;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of clone method, of class Triangle.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        Triangle expResult = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        Triangle result = instance.clone();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Triangle.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Triangle instance = new Triangle(new Dot(3,3), new Dot(0,3), new Dot(0,0));
        String expResult = "Прямоугольный\nПериметр: 10.242640687119284\nПлощадь: 4.499999999999997\nКоординаты точек:\nточка 1(3;3)\nточка 2(0;3)\nточка 3(0;0)\n";
        String result = instance.toString();
        assertEquals(expResult, result);
    }    
}
